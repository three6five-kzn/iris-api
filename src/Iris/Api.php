<?php
namespace Iris\Iris;

/* Description: Iris API library for PHP
 * Author: Allan Houston (allan.houston@three6five.com)
 */

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;

class Api {


    public function __construct($args){

        // Load the required extensions
        $this->basedir = isset($args['basedir']) ? $args['basedir'] : '';
        require $this->basedir.DIRECTORY_SEPARATOR.'vendor/autoload.php';

        // Set up caching for the returned JSON files to speed up repetive tasks

        $this->caching      = isset($args['caching'])   ? $args['caching']  : true;                               // Enable caching by default
        $this->cache_dir    = isset($args['cache_dir']) ? $args['cache_dir']  : dirname(__FILE__)."/cache";       // Cache to the scripts directory by default
        $this->cache_life   = isset($args['cache_life']) ? $args['cache_life']  : 86400;                          // Refresh the cache at least every day
        $this->apiUsername = isset($args['username']) ? $args['username'] : '';
        $this->apiPassword = isset($args['password']) ? $args['password'] : '';

        if (!($this->apiUsername && $this->apiPassword)) {
            print "Error: API username and password are required.\n";
            exit;
        }

        // Create a new Guzzle client
        $this->client = new Client([
            'base_uri' => isset($args['url']) ? $args['url'] : 'https://cloud.irisns.com/',          //  Default to cloud instance
            'timeout'  => 10.0,
            'cookies' => true
        ]);
        $this->cookieJar = $this->client->getConfig('cookies');
	 }

    // addDevice(args): Adds a new device to Iris
    public function addDevice($opt = []) {
      $arr = $this->addIrisDevice($opt);
    }

    // listClasses(args): Simple accessor for classes from Iris
    // TODO: The response is up to 100 classes - need to figure out what the query strings are to paginate or increase this
    public function listDevices($opt = []) {
      $arr = $this->getDataCache('device-list.json',function() { return $this->getIrisDevices();});
      return($arr['data']);
    }

    // listClasses(args): Simple accessor for classes from Iris
    // TODO: The response is up to 100 classes - need to figure out what the query strings are to paginate or increase this
    public function listDevicesView($opt = []) {
      $this->view = isset($opt['view']) ? $opt['view'] : '';
      if (!$this->view) { return([]);}
      $arr = $this->getDataCache('device-list-view.json',function() { return $this->getIrisDevicesView();});
      return($arr['data']);
    }


    // listClasses(args): Simple accessor for classes from Iris
    // TODO: The response is up to 100 classes - need to figure out what the query strings are to paginate or increase this
    public function listClasses($opt = []) {
      $arr = $this->getDataCache('device-classes.json',function() { return $this->getIrisClasses();});
      return($arr['data']);
    }

    // listVendors(args): Simple accessor for device vendors
    public function listVendorTypes($opt = []) {
      $arr = $this->getDataCache('device-vendor-types.json',function() { return $this->getIrisVendorTypes();});
      return($arr['data']);
    }

    // listPollers(args): Simple accessor for device vendors
    public function listPollers($opt = []) {
      $arr = $this->getDataCache('iris-pollers.json',function() { return $this->getIrisPollers();});
      return($arr['data']);
    }

    // listViews(args): Simple accessor for device vendors
    public function listViews($opt = []) {
      $arr = $this->getDataCache('iris-views.json',function() { return $this->getIrisViews();});
      return($arr['data']);
    }



    // listDeviceProfiles(args): Simple accessor for device vendors
    public function listDeviceProfiles($opt = []) {
      $arr = $this->getDataCache('get-device-profiles.json',function() { return $this->getDeviceProfiles();});
      return($arr['data']);
    }

    // listDeviceProfiles(args): Simple accessor for device vendors
    public function getPollerId($name,$cached=0) {
        $arr = $this->getDataCache('iris-pollers.json',function() { return $this->getIrisPollers();});
        foreach ($arr['data'] as $i => $rec) {
            if (isset($rec['hostname']) && (strtolower($rec['hostname'] == strtolower($name)))) {
              return($rec['poller_a']);
            }
        }
    }





    /* --------------------------------------------------
      |             Private Methods                      |
      ---------------------------------------------------*/

    // Fetch cached JSON data
    private function getDataCache($file,$function) {

      if (!file_exists($this->cache_dir)) { mkdir($this->cache_dir); }
      $jsonFile = sprintf("%s/%s",$this->cache_dir,$file);
      $lastUpdated = file_exists($jsonFile) ? filemtime($jsonFile) : time();
      $cacheValid = (time() - $lastUpdated < $this->cache_life) ? true : false;

      if ($this->caching && file_exists($jsonFile) && $cacheValid) {
        $json = file_get_contents($jsonFile);
        try {
          $data = json_decode($json,true);
          return($data);
        }
        catch(Exception $e) {
          error_log("Problem decoding cached data from device-classes.json: ".$e->getMessage());
        }
      }

      // Call the onward function and get it's data
      $this->irisLogin();
      $data = $function();

      //Encode the response and store in a JSON cache file
      $jsonData = json_encode($data,JSON_PRETTY_PRINT);
      file_put_contents($jsonFile,$jsonData);
      return($data);
    }

    // Private Function: getIrisClasses() - fetch the classes from the API
    private function getIrisDevices($options=[]) {
      $apiEndpoint = "iris/api2/api/devices";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }

    // Private Function: getIrisClasses() - fetch the classes from the API
    private function getIrisDevicesView($options=[]) {

      $apiEndpoint = sprintf('https://cloud.irisns.com/iris/api2/api/devices?prefetch=["poller_a_rel","poller_b_rel","deviceprofile",{"devices_views":"view"}]&iColumns=19&sColumns=,,,,,,,,,,,,,,,,,,&iDisplayStart=0&iDisplayLength=1000&mDataProp_16=view.code&sSearch_16=%s&bRegex_16=false&bSearchable_16=true&bSortable_16=true',$this->view);
      print "API: $apiEndpoint\n";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }


    // Private Function: getIrisClasses() - fetch the classes from the API
    private function getIrisClasses($options=[]) {
      $apiEndpoint = "iris/api2/api/device_class";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }

    // Private Function: getIrisVendorTypes() - fetch the vendors and types from the API
    private function getIrisVendorTypes($options=[]) {
      $apiEndpoint = "iris/api2/api/devicetypes";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }

    // Private Function: getIrisPollers() - fetch the vendors and types from the API
    private function getIrisPollers($options=[]) {
      $apiEndpoint = "iris/api2/api/pollers?ordered_by=hostname";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }

    // Private Function: getDeviceProfiles() - fetch the vendors and types from the API
    private function getDeviceProfiles($options=[]) {
      $apiEndpoint = "iris/api2/api/deviceprofiles?search.deleted=0&ordered_by=profilename";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }

    // Private Function: getIrisPollers() - fetch the vendors and types from the API
    private function getIrisViews($options=[]) {
      $apiEndpoint = "iris/api2/api/views?&iDisplayLength=-1";
      $response = $this->client->request('GET', $apiEndpoint);
      $data = json_decode($response->getBody(), true);
      return($data);
    }



    // Private Function: irisLogin() - log into Iris and store the resulting session cookie
    private function irisLogin() {

        $apiEndpoint = "iris/api2/login";
        $response = $this->client->request('POST', $apiEndpoint, [
            'form_params' => [
                "auth_type" => "local",
                "username"  => $this->apiUsername,
                "password"  => $this->apiPassword
            ]
        ]);

        // Check if we got the sessiontoken back from the API
        $cookieArray = $this->cookieJar->toArray()[0];
        if (isset($cookieArray['Name']) && ($cookieArray['Name']=='sessiontoken')) {
          error_log("Successfully logged into Iris API for user:".$this->apiUsername);
        }
        else {
          error_log("Error logging into the Iris API for user:".$this->apiUsername);
          error_log("Cookie array debug:".print_r($cookieArray,1));
        }

    }

}
